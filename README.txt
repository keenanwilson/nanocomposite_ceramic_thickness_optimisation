This directory contains macro files defining geometry, materials 
and surfaces for use with the Monte Carlo software package GATE.
Scripts for data analysis of that GATE output have been included
for use with MATLAB. Files have been tested with GATE version 8.1
and MATLAB version 2018a.

Copyright (C) 2019  Keenan Wilson, Daniel Franklin.

This is free code; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This code is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.



