%% --- Script to concatenate and plot all "photonfit" output
% num_primaries = number of primaries for each GATE run.

function concatData_SD_twosided(num_primaries)

% Directory to find all .dat files
ds = datastore([pwd '/*.dat'], 'FileExtensions','.dat', 'Type', 'tabulartext');

% Specifying parameters for the photonfit function.
det_dim_pix = [20 20];
det_dim_mm = [20 20];
atten_length = 100.0;
slab_thickness = 10.3; % including 0.3 mm for epoxy/meltmount.
fastmode = 1;
n_scint = 1.82;
n_epoxy = 1.52;
critical_angle_degrees = asind(n_epoxy/n_scint);
yield = 50000;

% Initial conditions for loop.
previousEvent = 0;
previousOutput = table;

% Loop takes each file in the directory, creates an output table for each
% with photonfit, then concatenates them into a large singular output.
for i = 1:numel(ds.Files)

    filename = ds.Files{i,1};

    output = photonfit_smallDetector_twosided(filename, det_dim_pix, ...
        det_dim_mm, atten_length, critical_angle_degrees, ...
        slab_thickness, yield, fastmode);
    
    % Change the event number to pick up from previous output.
    output.event = output.event + previousEvent;
    previousEvent = num_primaries * i; %max(output.event);
    
    % Concatenate new table with previous.
    previousOutput = vertcat(previousOutput,output);

end

% Save output table.
writetable(previousOutput, 'output.txt')

% Scatter plots and histograms for the output table.
locationError_SD_twosided (previousOutput, slab_thickness)

end