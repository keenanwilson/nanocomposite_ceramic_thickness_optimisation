%% Script to analyse detector output data

function [outputStats, outputComp] = detectorstats(inputTable)

% --- Median and mean error in X, Y and Z, IQR and Standard deviation -----

med_x = median(inputTable.Del_x);
med_y = median(inputTable.Del_y);
med_z = median(inputTable.Del_z);
med_all = median(inputTable.Del_d);

mean_x = mean(inputTable.Del_x);
mean_y = mean(inputTable.Del_y);
mean_z = mean(inputTable.Del_z);
mean_all = mean(inputTable.Del_d);

IQR_x = iqr(inputTable.Del_x);
IQR_y = iqr(inputTable.Del_y);
IQR_z = iqr(inputTable.Del_z);
IQR_all = iqr(inputTable.Del_d);

SD_x = std(inputTable.Del_x);
SD_y = std(inputTable.Del_y);
SD_z = std(inputTable.Del_z);
SD_all = std(inputTable.Del_d);

Error = {'X Error (mm)';'Y Error (mm)';'Z Error (mm)';'Total Error (mm)'};
Median = [med_x;med_y;med_z;med_all];
Mean = [mean_x;mean_y;mean_z;mean_all];
IQR = [IQR_x;IQR_y;IQR_z;IQR_all];
SD = [SD_x;SD_y;SD_z;SD_all];

outputStats = table(Median, Mean, IQR, SD, 'RowNames', Error)

% --- Percentage of comptons to photoelectric -----------------------------

photoE = sum(inputTable.compton == 0);
comp_1 = sum(inputTable.compton == 1);
comp_2 = sum(inputTable.compton == 2);
comp_3 = sum(inputTable.compton == 3);
comp_4 = sum(inputTable.compton == 4);
comp_5OM = sum(inputTable.compton >= 5);

percent_inter = sum(inputTable.event ~= 0) / 200000 * 100;
percent_photoE = photoE / sum(inputTable.compton ~= -1) * 100;
percent_comp_1 = comp_1 / sum(inputTable.compton ~= -1) * 100;
percent_comp_2 = comp_2 / sum(inputTable.compton ~= -1) * 100;
percent_comp_3 = comp_3 / sum(inputTable.compton ~= -1) * 100;
percent_comp_4 = comp_4 / sum(inputTable.compton ~= -1) * 100;
percent_comp_5OM = comp_5OM / sum(inputTable.compton ~= -1) * 100;

scatter = {'% Interaction';'% PhotoE';'% Comp1';'% Comp2';'% Comp3';'% Comp4';'% Comp5+'};
percent = [percent_inter;percent_photoE;percent_comp_1;percent_comp_2;percent_comp_3; ...
    percent_comp_4;percent_comp_5OM];

outputComp = table(percent, 'RowNames', scatter)

end
